import React from "react";
import ReactDOM from "react-dom";



let data = [[0,1,2,3,5,4],[2,0,1,3,5,4],[0,3,1,4,5,2],[1,3,2,4,5,0],[3,2,1,0,5,4],[5,4,3,1,2,0]]
let mData = [[0.1,0.2,0.1,0.2,0.1,0.3]]
let kData = [[0.5,0.1,0.1,0.1,0.1,0.1]]
let MessageCount = data[0].length
let KeyCount = data.length
let data1  = [[0,1,2,3,5,4],[2,0,1,3,5,4],[0,3,1,4,5,2],[1,3,2,4,5,0],[3,2,1,0,5,4],[5,4,3,1,2,0]],
	data2  = [[0,1,2,3,5,4],[2,0,1,3,5,4],[0,3,1,4,5,2],[1,3,2,4,5,0],[3,2,1,0,5,4],[5,4,3,1,2,0]],
	data3 = [[0,1,2,3,5,4],[2,0,1,3,5,4],[0,3,1,4,5,2],[1,3,2,4,5,0],[3,2,1,0,5,4],[5,4,3,1,2,0]],
	data4 = [[0,1,2,3,5,4],[2,0,1,3,5,4],[0,3,1,4,5,2],[1,3,2,4,5,0],[3,2,1,0,5,4],[5,4,3,1,2,0]]

function Cell(props) {
      return (
        <input
        type="number"
        defaultValue={props.myTable[props.row][props.col]}
        onChange={(e) => {
          props.myTable[props.row][props.col] = e.target.value
        }}/>
        )
  }

function ShowTable(props) {
	return(
		<table>
	      <tbody>
	      {
	        props.myTable.map((row, i) => (
	        <tr>
	        {row.map((_, j) => (<td><Cell row={i} col={j} myTable={props.myTable}/></td>))}
	        </tr>)
	      )
	    }
	    </tbody>
	    </table>
	)
}

function Fi (m,k,e){
	if (data[k][m]=== e) {return 1} else {return 0}
}

function Pem(m, e){
	return mData[0][m]*Pme(m ,e )/Pe(e)
}

function Pek(k,e){
	return kData[0][k]* Pke(k, e)/ Pe(e)
}

function Pme( m, e){
	let sum = 0
	for (let k = 0; k < kData[0].length; k++) {
		sum += kData[0][k] * Fi(m,k,e)
	}
	return sum
}

function Pe(e){
	let sum =0
	for (let m = 0; m < MessageCount; m++) {
		for (let k = 0; k < KeyCount; k++) {
			sum += kData[0][k]* mData[0][m]* Fi(m,k,e)
		}
	}
	return sum
}

function Pke(k, e){
	let sum = 0
	for (let m = 0; m < MessageCount; m++) {
		sum += mData[0][m]* Fi(m,k,e)
	}
	return sum
}

function PmeTable(myTable, func){
	for (let i = 0; i < myTable.length; i++) {
		for (let j = 0; j < myTable[i].length; j++) {
			myTable[i][j]= func(i,j)
		}
	}
	return myTable
}

function getM(k,e) {

	for (let m = 0; m < MessageCount; ++m) {
		if (Fi(m, k, e) === 1) {
			return m;
		}
	}
	return 0;
}


function App() {
  return (
    <div >
    P table
      <ShowTable myTable = {data} />
      <hr />
      M table
      <ShowTable myTable = {mData} />
      <hr />
      K table
      <ShowTable myTable = {kData} />
      <hr/>
      Pem table
      < ShowTable myTable = {PmeTable(data1, (e,m) => Pem(m, e))}/>
      <hr/>
      Pek table
      < ShowTable myTable = {PmeTable(data2, (e,k) => Pek(k, e))}/>
      <hr/>
      Pem - Pm
      < ShowTable myTable = {PmeTable(data3, (e,m) => data1[e][m] - mData[0][m])}/>
      <hr/>
      Pek / Pm
      < ShowTable myTable = {PmeTable(data4, (k,e) => data2[e][k] / mData[0][getM(k,e)])}/>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);