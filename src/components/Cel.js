import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {changeArr} from './actions'



class Cel extends Component{

   constructor(props)
    {
      super(props);
      this.state ={
        pos : props.cel.pos,
        num: props.cel.num,
        tableType: props.tableType
      
      }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {

    this.setState({num: event.target.value});
  }

  handleSubmit(event) {
  	this.props.changeArr(this.state.pos, this.state.num, this.state.tableType)
    
    event.preventDefault();
  }
  render(){
  
        
    return (
      <div>

      Cell pos {this.state.pos} 
      
 		 <form onSubmit={this.handleSubmit}>
        <label>
          num:
          <input type="text" value={this.state.num} onChange={this.handleChange} />
        </label>
        <input type="submit" value="send" />
      </form>
      </div>
      )
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({changeArr: changeArr},dispatch)
}

export default connect(null, mapDispatchToProps )(Cel)