import React, {Component} from 'react'
import Cel from './Cel'
import {connect} from 'react-redux'


class Tabl extends Component{
createFullTable = () => {
  if (this.props.arr.fullTable.length >0 ) {
    let table = []
    for (let i = 0; i < 5; i++) {
      let children = []
      for (let j = 0; j < 5; j++) {
        children.push(<td>
          <Cel cel={this.props.arr.fullTable[i*6+j]} tableType ='fullTable' />
        </td>)
      }
     
      table.push(<tr>{children } </tr>)
          }
    return table
  }
}
  createMTable = () => {
  if (this.props.arr.mTable.length >0 ) {
    let table = []
    for (let i = 0; i < 1; i++) {
      let children = []
      for (let j = 0; j < 6; j++) {
        children.push(<td>
          <Cel cel={this.props.arr.mTable[j]} tableType ='mTable' />
        </td>)
      }
     
      table.push(<tr>{children } </tr>)
          }
    return table
  }
}
    createKTable = () => {
  if (this.props.arr.kTable.length >0 ) {
    let table = []
    for (let i = 0; i < 1; i++) {
      let children = []
      for (let j = 0; j < 6; j++) {
        children.push(<td>
          <Cel cel={this.props.arr.kTable[j]} tableType= 'kTable' />
        </td>)
      }
     
      table.push(<tr>{children } </tr>)
          }
    return table
  }
    
    
    
  }
  render(){
      return (
      <div>
      таблиця повідомлень
    {this.createFullTable()}
    <hr/>
    таблиця М
     {this.createMTable()} 
     <hr/>
     таблиця К
     {this.createKTable()} 

     {console.log(this.props.arr.fullTable)} 
     {console.log(this.props.arr.mTable)}
     {console.log(this.props.arr.kTable)}  
      </div>
      )
  }
}



function mapStateToProps(state){
  return{
    arr: state
  }
}

export default connect (mapStateToProps)(Tabl)



